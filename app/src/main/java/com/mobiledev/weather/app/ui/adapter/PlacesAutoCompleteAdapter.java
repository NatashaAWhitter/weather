package com.mobiledev.weather.app.ui.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import com.mobiledev.weather.app.utils.LocationUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Natasha Whitter
 * @since 17/05/2014
 */
public class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable
{
    private ArrayList<String> resultList = new ArrayList<String>();

    public PlacesAutoCompleteAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public String getItem(int index) {
        if (resultList.size() != 0 || resultList != null) {
            return resultList.get(index);
        }
        return "";
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    // Retrieve the autocomplete results
                    resultList = LocationUtils.autocomplete(constraint.toString());

                    // Assign the data to the FilterResults
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }
}
