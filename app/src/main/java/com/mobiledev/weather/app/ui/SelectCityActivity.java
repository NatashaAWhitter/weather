package com.mobiledev.weather.app.ui;

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.view.*;
import android.widget.*;
import com.mobiledev.weather.app.R;
import com.mobiledev.weather.app.ui.adapter.PlacesAutoCompleteAdapter;

/**
 * @author Natasha Whitter
 * @since 17/05/2014
 */
public class SelectCityActivity extends ListActivity implements AdapterView.OnItemClickListener
{
    private ArrayAdapter<String> placesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setHasOptionsMenu(true);

        if (getActionBar() != null)
        {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        placesAdapter = new PlacesAutoCompleteAdapter(this, android.R.layout.simple_list_item_1);
        setListAdapter(placesAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getListView().setTextFilterEnabled(true);
        getListView().setOnItemClickListener(this);
    }

    /**
     * Setup search view
     * @param menu
     * @return
     * @see "http://stackoverflow.com/questions/16847514/execute-search-from-action-bar"
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.clear();
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView view = (SearchView) menu.findItem(R.id.search).getActionView();

        view.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        view.setIconifiedByDefault(false);

        SearchView.OnQueryTextListener query = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                placesAdapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                placesAdapter.getFilter().filter(newText);
                return true;
            }
        };
        view.setOnQueryTextListener(query);
        return true;
    }

    /**
     * http://developer.android.com/training/implementing-navigation/ancestral.html
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String city = (String) parent.getItemAtPosition(position);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.pref_selected_location), city);
        if (editor.commit()) {
            if (sharedPref.getBoolean(getString(R.string.pref_current_location_key), true)) {
                displayMessage();
            }
            else
            {
                finish();
            }
        }
    }

    public void displayMessage()
    {
            new AlertDialog.Builder(this).setTitle("Current location is turned on")
                    .setMessage("Current location is turned off in settings > refresh and location " +
                            " would you like to go to settings?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(SelectCityActivity.this, PrefsActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    })
                     // Button doesn't need to do anything
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            NavUtils.navigateUpFromSameTask(SelectCityActivity.this);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
    }
}
