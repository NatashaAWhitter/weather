package com.mobiledev.weather.app.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobiledev.weather.app.ApplicationConstants;
import com.mobiledev.weather.app.R;
import com.mobiledev.weather.app.data.HourlyForecast;
import com.mobiledev.weather.app.data.HourlyForecastWeather;
import com.mobiledev.weather.app.utils.WeatherUtils;

import java.text.SimpleDateFormat;

/**
 * @author Natasha Whitter
 * @since 19/05/2014
 */
public class HourlyForecastAdapter extends BaseAdapter {

    private HourlyForecastWeather weather;
    private Context context;

    public HourlyForecastAdapter(Context context, HourlyForecastWeather weather) {
        this.context = context;
        this.weather = weather;
    }

    @Override
    public int getCount() {
        if (weather != null)
        {
            return weather.forecasts.size();
        }
        return 0;
    }

    @Override
    public HourlyForecast getItem(int position) {
        if (weather != null)
        {
            return weather.forecasts.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (weather != null)
        {
            return position;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.forecast_list_item, null);
        }

        HourlyForecast forecast = weather.forecasts.get(position);

        RelativeLayout rlList = (RelativeLayout) convertView.findViewById(R.id.list);
        ImageView icon = (ImageView) convertView.findViewById(R.id.icon);
        TextView tvTitle = (TextView) convertView.findViewById(R.id.time);
        TextView tvTemp = (TextView) convertView.findViewById(R.id.temp);
        TextView tvWindSpeed = (TextView) convertView.findViewById(R.id.wind_speed);

        rlList.setBackgroundColor(WeatherUtils.getBackgroundColor(forecast.temperature.getTemperature(), context));
        icon.setImageResource(WeatherUtils.getIcon(forecast.condition.getIcon()));
        SimpleDateFormat dateFormat = new SimpleDateFormat(ApplicationConstants.DEFAULT_TIME_24HR);
        tvTitle.setText(dateFormat.format(forecast.condition.getDate()));

        tvTemp.setText(WeatherUtils.getTemperature(forecast.temperature.getTemperature(), context));
        tvWindSpeed.setText(WeatherUtils.getWindSpeed(forecast.wind.getSpeed(), context));

        return convertView;
    }
}
