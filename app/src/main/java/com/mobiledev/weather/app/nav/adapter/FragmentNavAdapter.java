package com.mobiledev.weather.app.nav.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobiledev.weather.app.R;
import com.mobiledev.weather.app.nav.data.FragmentNavItem;

import java.util.ArrayList;

/**
 * @author Natasha Whitter
 * @since 06/05/2014
 */
public class FragmentNavAdapter extends BaseAdapter
{
    private Context context;
    private ArrayList<FragmentNavItem> items;

    public FragmentNavAdapter(Context context, ArrayList<FragmentNavItem> items)
    {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount()
    {
        return items.size();
    }

    @Override
    public Object getItem(int position)
    {
        return items.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
            if (convertView == null)
            {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.drawer_list_item, null);
            }
            TextView txtTitle = (TextView) convertView.findViewById(R.id.time);

            txtTitle.setText(items.get(position).getTitle());

            return convertView;
    }

    public void add(FragmentNavItem fragmentNavItem)
    {
        items.add(fragmentNavItem);
    }
}
