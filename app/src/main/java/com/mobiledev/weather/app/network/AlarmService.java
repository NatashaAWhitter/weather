package com.mobiledev.weather.app.network;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import com.mobiledev.weather.app.R;

/**
 * @author Natasha Whitter
 * @since 08/05/2014
 * @version 1.0
 */
public class AlarmService extends Service
{
    private static final String TAG = "AlarmService";
    private int REQUEST_CODE = 1;
    private AlarmManager manager;
    private Intent intent;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        int classNumber = 0;

        if (intent != null) {
            classNumber = intent.getIntExtra("weatherType", 0);
            Log.d(TAG, String.valueOf(classNumber));
        }

        Log.d(TAG, "Service created");
        super.onCreate();

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean autoUpdate = sharedPref.getBoolean(getBaseContext().getString(R.string.pref_auto_update_key), true);
        int updateFrequency = Integer.parseInt(sharedPref.getString(getBaseContext().getString(R.string.pref_update_frequency_key), "0"));

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("weatherType", classNumber);
        editor.commit();

        this.intent = new Intent(this, WeatherClient.class);
        manager = (AlarmManager) getSystemService(ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, REQUEST_CODE, this.intent, 0);

        if (autoUpdate || updateFrequency != -1) {
            Log.d(TAG, "Update regularly");
            manager.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(), getUpdateInterval(updateFrequency), pendingIntent);
        }
        else
        {
            Log.d(TAG, "Update once");
            manager.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(), pendingIntent);
        }
        Log.d(TAG, "Service done");

        return super.onStartCommand(this.intent, flags, startId);
    }

    public long getUpdateInterval(int frequency)
    {
        switch (frequency)
        {
            case 0:
                return 30000L; // 30 seconds
            case 1:
                return AlarmManager.INTERVAL_HOUR; // 1 hour 3600000L
            case 2:
                return AlarmManager.INTERVAL_HOUR * 4; // 4 hour 14400000L
            case 3:
                return AlarmManager.INTERVAL_HALF_DAY/2; // 6 hours 21600000L
            case 4:
                return AlarmManager.INTERVAL_HALF_DAY; // 12 hours 43200000L
            default:
                return 30000L;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (manager != null)
        {
            manager.cancel(PendingIntent.getBroadcast(this, REQUEST_CODE, intent, 0));
        }
        Log.d(TAG, "Service destroyed");
    }
}
