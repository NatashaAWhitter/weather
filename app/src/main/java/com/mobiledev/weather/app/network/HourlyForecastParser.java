package com.mobiledev.weather.app.network;

import android.os.AsyncTask;
import android.util.Log;
import com.mobiledev.weather.app.data.HourlyForecastWeather;
import com.mobiledev.weather.app.network.interfaces.AsyncResponse;
import com.mobiledev.weather.app.network.utils.ParserUtils;

/**
 * @author Natasha Whitter
 * @since 19/05/2014
 */
public class HourlyForecastParser extends AsyncTask<Double, Void, HourlyForecastWeather> {
    private static final String TAG = "HourlyForecastParser";
    public static AsyncResponse response = null;

    @Override
    protected HourlyForecastWeather doInBackground(Double... params) {
        HourlyForecastWeather weather = getWeather(params[0], params[1]);
            Log.d(TAG, weather.toString());
        return weather;
    }

    @Override
    protected void onPostExecute(HourlyForecastWeather weather) {
        if (response != null)
        {
            response.processFinish(weather);
        }
    }

    private HourlyForecastWeather getWeather(double longitude, double latitude)
    {
        return ParserUtils.getHourlyForecast(new WeatherConnection().getHourlyForecast(longitude, latitude));
    }
}
