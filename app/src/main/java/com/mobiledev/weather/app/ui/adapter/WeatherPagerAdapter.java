package com.mobiledev.weather.app.ui.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import com.mobiledev.weather.app.ui.CurrentWeatherFragment;
import com.mobiledev.weather.app.ui.DailyForecastFragment;
import com.mobiledev.weather.app.ui.HourlyForecastFragment;

/**
 * @author Natasha Whitter
 * @since 20/05/2014
 */
public class WeatherPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 3;

    public WeatherPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new CurrentWeatherFragment();
            case 1:
                return new DailyForecastFragment();
            case 2:
                return new HourlyForecastFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }
}
