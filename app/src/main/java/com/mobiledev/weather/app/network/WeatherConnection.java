package com.mobiledev.weather.app.network;

import android.util.Log;
import com.mobiledev.weather.app.ApplicationConstants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author Natasha Whitter
 * @since 10/05/2014
 */
public class WeatherConnection {
    private static final String TAG = "WeatherConnection";

    public String getCurrentWeather(double longitude, double latitude)
    {
        return getWeather(ApplicationConstants.BASE_URL + ApplicationConstants.LAT_QUERY + latitude + ApplicationConstants.LON_QUERY + longitude + ApplicationConstants.JSON_MODE + ApplicationConstants.LANG_ENG);
    }

    public String getDailyForecast(double longitude, double latitude)
    {
        return getWeather(ApplicationConstants.BASE_DAILY_FORECAST_URL + ApplicationConstants.LAT_QUERY + latitude + ApplicationConstants.LON_QUERY + longitude + ApplicationConstants.DAY_QUERY + ApplicationConstants.JSON_MODE + ApplicationConstants.LANG_ENG);
    }

    public String getHourlyForecast(double longitude, double latitude)
    {
        return getWeather(ApplicationConstants.BASE_FORECAST_URL + ApplicationConstants.LAT_QUERY + latitude + ApplicationConstants.LON_QUERY + longitude + ApplicationConstants.JSON_MODE + ApplicationConstants.LANG_ENG);
    }

    public String getWeather(String url) {
        HttpURLConnection connection;
        InputStream stream;

        try {
            connection = (HttpURLConnection) (new URL(url)).openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.connect();

            // Read the response
            StringBuilder builder = new StringBuilder();
            stream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

            String line;
            while ((line = reader.readLine()) != null)
            {
                builder.append(line + "\r\n");
            }

            stream.close();
            connection.disconnect();

            Log.d(TAG, "Received weather data [" + builder.toString() + "]");
            return builder.toString();
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
