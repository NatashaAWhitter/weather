package com.mobiledev.weather.app.data;

import com.mobiledev.weather.app.data.interfaces.BaseLocation;
import com.mobiledev.weather.app.data.interfaces.BaseWeather;

import java.util.ArrayList;

/**
 * @author Natasha Whitter
 * @since 09/05/2014
 */
public class DailyForecastWeather extends BaseWeather{
    public Location location;
    public ArrayList<DailyForecast> forecasts;

    @Override
    public String toString() {
        return "ForecastWeather{" +
                "id=" + id +
                ", cod=" + cod +
                ", message='" + message + '\'' +
                ", forecasts=" + forecasts +
                '}';
    }

    public DailyForecastWeather() {
        location = new Location();
        forecasts = new ArrayList<DailyForecast>();
    }

    public class Location extends BaseLocation
    {
        private int days;

        public int getDays() {
            return days;
        }

        public void setDays(int days) {
            this.days = days;
        }

        @Override
        public String toString() {
            return "Location{" +
                    "longitude=" + longitude +
                    ", latitude=" + latitude +
                    ", sunrise=" + sunrise +
                    ", sunset=" + sunset +
                    ", city='" + city + '\'' +
                    ", country='" + country + '\'' +
                    '}';
        }
    }
}
