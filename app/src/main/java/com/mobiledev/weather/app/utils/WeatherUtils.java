package com.mobiledev.weather.app.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.mobiledev.weather.app.R;

import java.io.Serializable;

/**
 * @author Natasha Whitter
 * @since 13/05/2014
 *
 * http://programers-guide.blogspot.co.uk/2012/06/java-programs.html
 */
public class WeatherUtils {

    /**
     * This method converts the kelvin temperature to the celsius equivalent
     * @param kelvin - temperature to be converted into celsius
     * @return the celsius value rounded
     */
    public static int kelvinToCelsius(Double kelvin)
    {
        return (int) Math.round(kelvin - 273.15);
    }

    /**
     * This method converts the kelvin temperature to the fahrenheit equivalent using the celsius conversion method
     * @param kelvin - temperature to be converted into fahrenheit
     * @return the fahrenheit value rounded
     */
    public static int kelvinToFahrenheit(Double kelvin)
    {
        return (int) Math.round(kelvinToCelsius(kelvin) * 9.0/5.0 + 32.0);
    }

    /**
     * This method converts the kilopascal pressure to the bar equivalent
     * @param kilopascal - temperature to be converted into bar
     * @return the bar value rounded
     */
    public static int kilopascalToBar(Double kilopascal)
    {
        return (int) Math.round(kilopascal * 0.01);
    }

    /**
     * This method converts the kilopascal pressure to the torr equivalent
     * @param kilopascal - temperature to be converted into torr
     * @return the torr value rounded
     */
    public static int kilopascalToTorrs(Double kilopascal)
    {
        return (int) Math.round(kilopascal * 7.5);
    }

    /**
     * This method converts the kilopascal pressure to the pascal equivalent
     * @param kilopascal - temperature to be converted into pascal
     * @return the pascal value rounded
     */
    public static int kilopascalToPascal(Double kilopascal)
    {
        return (int) Math.round(kilopascal * 1000);
    }

    /**
     * This method converts the miles per hour to the kilometres per hour
     * @param speed - miles per hour to be converted
     * @return the kilometres per hour rounded
     */
    public static int milesToKilometres(Double speed)
    {
        return (int) Math.round(speed * 1.61);
    }

    /**
     * This method finds the matching weather image
     * @param icon - used to select the correct weather image
     * @return the drawable weather image
     */
    public static int getIcon(String icon)
    {
        if (icon.equals("01d"))
        {
            return R.drawable.day_clear;
        }
        else if (icon.equals("02d"))
        {
            return R.drawable.day_few_clouds;
        }
        else if (icon.equals("03d") || icon.equals("03n"))
        {
            return R.drawable.day_scattered_clouds;
        }
        else if (icon.equals("04d") || icon.equals("04n"))
        {
            return R.drawable.day_broken_clouds;
        }
        else if (icon.equals("09d"))
        {
            return R.drawable.day_shower_rain;
        }
        else if (icon.equals("10d"))
        {
            return R.drawable.day_rain;
        }
        else if (icon.equals("11d"))
        {
            return R.drawable.day_thunderstorm;
        }
        else if (icon.equals("13d"))
        {
            return R.drawable.day_snow;
        }
        else if (icon.equals("50d"))
        {
            return R.drawable.day_mist;
        }
        else if (icon.equals("01n"))
        {
            return R.drawable.night_clear;
        }
        else if (icon.equals("02n"))
        {
            return R.drawable.night_few_clouds;
        }
        else if (icon.equals("09n"))
        {
            return R.drawable.night_shower_rain;
        }
        else if (icon.equals("10n"))
        {
            return R.drawable.night_rain;
        }
        else if (icon.equals("11n"))
        {
            return R.drawable.night_thunderstorm;
        }
        else if (icon.equals("13n"))
        {
            return R.drawable.night_snow;
        }
        else if (icon.equals("50n"))
        {
            return R.drawable.night_mist;
        }
        return R.drawable.weather_empty;
    }

    /**
     * Depending on the chosen temperature type in shared preferences, temperature will be converted into the type selected
     * @param temp - the temperature used to convert into a different type
     * @param context - the context received from the calling activity, so shared preferences can be accessed
     * @return string value of the converted temperature
     */
    public static String getTemperature(Double temp, Context context)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        switch (Integer.parseInt(sharedPref.getString(context.getString(R.string.pref_temp_key), "0")))
        {
            case 0:
                return WeatherUtils.kelvinToCelsius(temp) + "C";
            case 1:
                return WeatherUtils.kelvinToFahrenheit(temp) + "F";
            case 2:
                return ((int) Math.round(temp)) + "K";
            default:
                return WeatherUtils.kelvinToCelsius(temp) + "C";
        }
    }

    /**
     * Depending on the chosen pressure type in shared preferences, pressure will be converted into the type selected
     * @param pressure - the pressure used to convert into a different type
     * @param context - the context received from the calling activity, so shared preferences can be accessed
     * @return string value of the converted pressure
     */
    public static int getPressure(Double pressure, Context context)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        switch (Integer.parseInt(sharedPref.getString(context.getString(R.string.pref_pressure_key), "0")))
        {
            case 0:
                return (int) Math.round(pressure);
            case 1:
                return kilopascalToBar(pressure);
            case 2:
                return kilopascalToTorrs(pressure);
            case 3:
                return kilopascalToPascal(pressure);
            default:
                return kilopascalToBar(pressure);
        }
    }

    /**
     * Depending on the chosen speed type in shared preferences, speed will be converted into the type selected
     * @param speed - the speed used to convert into miles or kilometres per hour
     * @param context - the context received from the calling activity, so shared preferences can be accessed
     * @return string value of the converted speed
     */
    public static String getWindSpeed(Double speed, Context context)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        switch (Integer.parseInt(sharedPref.getString(context.getString(R.string.pref_wind_key), "0")))
        {
            case 0:
                return ((int) Math.round(speed)) + "mph/h";
            case 1:
                return (milesToKilometres(speed) + "kph/h");
            default:
                return ((int) Math.round(speed)) + "mph/h";
        }
    }

    /**
     * Using the wind degree, an image is selected
     * @param windDegree - the wind degree used to find the correct image
     * @return reference to drawable
     */
    @Deprecated
    public static int getDegreeIcon(Double windDegree)
    {
        if ((windDegree >= 0 && windDegree < 22.5) || (windDegree >= 337.5 && windDegree <= 360.0))
        {
            return R.drawable.degree_north;
        }
        else if (windDegree >= 22.5 && windDegree < 67.5)
        {
            return R.drawable.degree_north_east;
        }
        else if (windDegree >= 67.5 && windDegree < 112.5)
        {
            return R.drawable.degree_east;
        }
        else if (windDegree >= 112.5 && windDegree < 157.5)
        {
            return R.drawable.degree_south_east;
        }
        else if (windDegree >= 157.5 && windDegree < 202.5)
        {
            return R.drawable.degree_south;
        }
        else if (windDegree >= 202.5 && windDegree < 247.5)
        {
            return R.drawable.degree_south_west;
        }
        else if (windDegree >= 247.5 && windDegree < 292.5)
        {
            return R.drawable.degree_west;
        }
        else if (windDegree >= 292.5 && windDegree < 337.5)
        {
            return R.drawable.degree_north_west;
        }
        return R.drawable.degree_north;
    }

    public static String getDegrees(Double windDegree)
    {
        if ((windDegree >= 0 && windDegree < 22.5) || (windDegree >= 337.5 && windDegree <= 360.0))
        {
            return "North";
        }
        else if (windDegree < 67.5)
        {
            return "North East";
        }
        else if (windDegree < 112.5)
        {
            return "East";
        }
        else if (windDegree < 157.5)
        {
            return "South East";
        }
        else if (windDegree < 202.5)
        {
            return "South";
        }
        else if (windDegree < 247.5)
        {
            return "South West";
        }
        else if (windDegree < 292.5)
        {
            return "West";
        }
        else if (windDegree < 337.5)
        {
            return "North East";
        }
        return "";
    }

    public static int getBackgroundColor(double temp, Context context) {
        if (temp <= 282.15) {
            return context.getResources().getColor(R.color.color_very_cold);
        } else if (temp <= 284.15) {
            return context.getResources().getColor(R.color.color_cold);
        } else if (temp <= 293.15) {
            return context.getResources().getColor(R.color.color_warm);
        } else if (temp <= 313.15) {
            return context.getResources().getColor(R.color.color_hot);
        } else {
            return context.getResources().getColor(R.color.color_very_hot);
        }
    }
}
