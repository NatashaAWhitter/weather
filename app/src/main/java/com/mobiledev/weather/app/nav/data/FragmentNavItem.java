package com.mobiledev.weather.app.nav.data;

import android.app.Fragment;
import android.os.Bundle;

/**
 * @author Nathan Esquenazi <Nathan Esquenazi>
 * @version 1.0
 * @since 08/01/2014
 */
public class FragmentNavItem
{
    private Class<? extends Fragment> fragmentClass;
    private String title;
    private Bundle fragmentArgs;

    public FragmentNavItem(String title, Class<? extends Fragment> fragmentClass)
    {
        this(title, fragmentClass, null);
    }

    public FragmentNavItem(String title, Class<? extends Fragment> fragmentClass, Bundle args)
    {
        this.fragmentClass = fragmentClass;
        this.fragmentArgs = args;
        this.title = title;
    }

    public Class<? extends Fragment> getFragmentClass()
    {
        return fragmentClass;
    }

    public String getTitle()
    {
        return title;
    }

    public Bundle getFragmentArgs()
    {
        return fragmentArgs;
    }
}