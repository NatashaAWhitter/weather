package com.mobiledev.weather.app.data.interfaces;

import java.io.Serializable;

/**
 * @author Natasha Whitter
 * @since 20/05/2014
 */
public abstract class BaseClouds implements Serializable{
    protected int all;

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }

    @Override
    public String toString() {
        return "Clouds{" +
                "all=" + all +
                '}';
    }
}
