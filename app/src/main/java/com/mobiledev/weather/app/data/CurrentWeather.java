package com.mobiledev.weather.app.data;

import com.j256.ormlite.table.DatabaseTable;
import com.mobiledev.weather.app.data.interfaces.*;

import java.io.Serializable;

/**
 * @author Natasha Whitter
 * @since 09/05/2014
 */
public class CurrentWeather extends BaseWeather implements Serializable {

    public Location location;
    public Condition condition;
    public Temperature temperature;
    public Wind wind;
    public Rain rain;
    public Snow snow;
    public Clouds clouds;

    public CurrentWeather() {
        condition = new Condition();
        location = new Location();
        condition = new Condition();
        temperature = new Temperature();
        wind = new Wind();
        rain = new Rain();
        snow = new Snow();
        clouds = new Clouds();
    }

    @Override
    public String toString() {
        return "CurrentWeather{" +
                "location=" + location +
                ", condition=" + condition +
                ", temperature=" + temperature +
                ", wind=" + wind +
                ", rain=" + rain +
                ", snow=" + snow +
                ", scattered_clouds=" + clouds +
                ", id=" + id +
                ", cod=" + cod +
                ", message='" + message + '\'' +
                '}';
    }

    public class Location extends BaseLocation
    {
        private long datetime;

        public long getDatetime() {
            return datetime;
        }

        public void setDatetime(long datetime) {
            this.datetime = datetime;
        }

        @Override
        public String toString() {
            return "Location{" +
                    "id=" + getId() +
                    ", longitude=" + longitude +
                    ", latitude=" + latitude +
                    ", sunrise=" + sunrise +
                    ", sunset=" + sunset +
                    ", city='" + city + '\'' +
                    ", country='" + country + '\'' +
                    ", datetime=" + datetime +
                    '}';
        }

    }

    public class Condition extends BaseCondition
    {
    }

    public class Temperature extends BaseTemperature
    {
    }

    public class Wind extends BaseWind
    {
        private double gust;

        public double getGust() {
            return gust;
        }

        public void setGust(double gust) {
            this.gust = gust;
        }

        @Override
        public String toString() {
            return "Wind{" +
                    "speed=" + speed +
                    ", degree=" + degree +
                    ", gust=" + gust +
                    '}';
        }
    }

    public class Rain extends BaseRain
    {
    }

    public class Snow extends BaseSnow
    {
    }

    public class Clouds extends BaseClouds
    {
    }
}
