package com.mobiledev.weather.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.mobiledev.weather.app.network.AlarmService;

/**
 * Starts the weather service when the device has booted
 *
 * @author Natasha Whitter
 * @since 08/05/2014
 * @version 1.0
 */
public class OnBootReceiver extends BroadcastReceiver
{
    private static final String TAG = "OnBootReceiver";

    /**
     * If the intent passed is BOOT_COMPLETED action, then start the weather service
     *
     * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
     * @param context The Context in which the receiver is running.
     * @param intent  The Intent being received.
     */
    @Deprecated
    @Override
    public void onReceive(Context context, Intent intent)
    {
        // If intent is the BOOT_COMPLETED action, then start the service
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED"))
        {
            Intent i = new Intent(context, AlarmService.class);
            context.startService(i);
            Log.d(TAG, "Service started on device boot");
        }
    }
}
