package com.mobiledev.weather.app.network.utils;

import android.util.Log;
import com.mobiledev.weather.app.ApplicationConstants;
import com.mobiledev.weather.app.data.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Natasha Whitter
 * @since 21/05/2014
 */
public class ParserUtils {
    private static final String TAG = "ParserUtils";

    public static CurrentWeather getCurrentWeather(String data) {
        CurrentWeather weather = new CurrentWeather();
        try
        {
            JSONObject object = new JSONObject(data);
            weather.setCod(object.getInt("cod"));

            if (weather.getCod() == 200) {
                Log.d(TAG, "City data received");
                JSONObject jCoords = object.getJSONObject("coord");
                weather.location.setLongitude(jCoords.getDouble("lon"));
                weather.location.setLatitude(jCoords.getDouble("lat"));
                weather.location.setCity(object.getString("name"));
                weather.location.setId(object.getInt("id"));
                weather.location.setDatetime(new Date().getTime());

                JSONObject jSys = object.getJSONObject("sys");
                weather.location.setSunrise(jSys.getInt("sunrise"));
                weather.location.setSunset(jSys.getInt("sunset"));
                weather.location.setCountry(jSys.getString("country"));

                // Get the first value from the current weather
                JSONArray jWeather = object.getJSONArray("weather");
                JSONObject jCurrent = jWeather.getJSONObject(0);
                weather.condition.setId(jCurrent.getInt("id"));
                weather.condition.setCondition(jCurrent.getString("main"));
                weather.condition.setDescription(jCurrent.getString("description"));
                weather.condition.setIcon(jCurrent.getString("icon"));

                JSONObject jDetails = object.getJSONObject("main");
                weather.condition.setHumidity(jDetails.getDouble("humidity"));
                weather.condition.setPressure(jDetails.getDouble("pressure"));
                weather.temperature.setTemperature(jDetails.getDouble("temp"));
                weather.temperature.setMinTemperature(jDetails.getDouble("temp_min"));
                weather.temperature.setMaxTemperature(jDetails.getDouble("temp_max"));

                JSONObject jWind = object.getJSONObject("wind");
                weather.wind.setSpeed(jWind.getDouble("speed"));
                weather.wind.setDegree(jWind.getDouble("deg"));

                JSONObject jClouds = object.getJSONObject("clouds");
                weather.clouds.setAll(jClouds.getInt("all"));

                if (jCurrent.getString("main").equals("Rain")) {
                    JSONObject jRain = object.getJSONObject("rain");
                    weather.rain.setAmount(jRain.getDouble("1h"));
                }

                if (jCurrent.getString("main").equals("Snow")) {
                    JSONObject jSnow = object.getJSONObject("snow");
                    weather.snow.setAmount(jSnow.getDouble("1h"));
                }
            }
            else
            {
                weather.setMessage(object.getString("message"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            weather.setCod(400);
            weather.setMessage(e.getMessage());
        }

        return weather;
    }

    public static DailyForecastWeather getDailyForecast(String data)
    {
        DailyForecastWeather weather = new DailyForecastWeather();
        try {
            JSONObject object = new JSONObject(data);
            weather.setCod(object.getInt("cod"));

            if (weather.getCod() == 200) {
                Log.d(TAG, weather.toString());
                JSONObject jCity = object.getJSONObject("city");
                weather.location.setId(jCity.getInt("id"));
                weather.location.setCity(jCity.getString("name"));
                weather.location.setCountry(jCity.getString("country"));
                weather.location.setDays(object.getInt("cnt"));

                JSONObject jCoords = jCity.getJSONObject("coord");
                weather.location.setLongitude(jCoords.getDouble("lon"));
                weather.location.setLatitude(jCoords.getDouble("lat"));

                JSONArray jWeather = object.getJSONArray("list");

                for (int i = 0; i < jWeather.length(); i++) {
                    JSONObject jCurrent = jWeather.getJSONObject(i);
                    DailyForecast forecast = new DailyForecast();

                    forecast.condition.setHumidity(jCurrent.getDouble("humidity"));
                    forecast.condition.setPressure(jCurrent.getDouble("pressure"));
                    forecast.condition.setDatetime(jCurrent.getLong("dt"));

                    JSONObject jDetails = jCurrent.getJSONArray("weather").getJSONObject(0);

                    forecast.condition.setId(jDetails.getInt("id"));
                    forecast.condition.setDescription(jDetails.getString("description"));
                    forecast.condition.setCondition(jDetails.getString("main"));
                    forecast.condition.setIcon(jDetails.getString("icon"));

                    JSONObject jTemp = jCurrent.getJSONObject("temp");

                    forecast.temperature.setTemperature(jTemp.getDouble("day"));
                    forecast.temperature.setMinTemperature(jTemp.getDouble("min"));
                    forecast.temperature.setMaxTemperature(jTemp.getDouble("max"));
                    forecast.temperature.setNightTemperature(jTemp.getDouble("night"));
                    forecast.temperature.setEveTemperature(jTemp.getDouble("eve"));
                    forecast.temperature.setMornTemperature(jTemp.getDouble("morn"));

                    forecast.wind.setSpeed(jCurrent.getDouble("speed"));
                    forecast.wind.setDegree(jCurrent.getDouble("deg"));
                    forecast.clouds.setAll(jCurrent.getInt("clouds"));

                    try {
                        forecast.rain.setAmount(jCurrent.getDouble("rain"));
                    } catch (JSONException e) {
                        Log.d(TAG, "No value for rain");
                    }

                    try {
                        forecast.snow.setAmount(jCurrent.getDouble("snow"));
                    } catch (JSONException e) {
                        Log.d(TAG, "No value for snow");
                    }

                    weather.forecasts.add(forecast);
                }
            } else {
                weather.setMessage(object.getString("message"));
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            weather = new DailyForecastWeather();
            weather.setCod(400);
            weather.setMessage(e.getMessage());
        }

        return weather;
    }

    public static HourlyForecastWeather getHourlyForecast(String data)
    {
        HourlyForecastWeather weather = new HourlyForecastWeather();

        try {
            JSONObject object = new JSONObject(data);
            weather.setCod(object.getInt("cod"));

            if (weather.getCod() == 200) {
                JSONObject jCity = object.getJSONObject("city");
                weather.location.setId(jCity.getInt("id"));
                weather.location.setCity(jCity.getString("name"));
                weather.location.setCountry(jCity.getString("country"));
                weather.location.setDays(object.getInt("cnt"));

                JSONObject jCoords = jCity.getJSONObject("coord");
                weather.location.setLongitude(jCoords.getDouble("lon"));
                weather.location.setLatitude(jCoords.getDouble("lat"));

                JSONArray jWeather = object.getJSONArray("list");

                for (int i = 0; i < jWeather.length(); i++) {
                    JSONObject jCurrent = jWeather.getJSONObject(i);
                    HourlyForecast forecast = new HourlyForecast();

                    forecast.condition.setDatetime(jCurrent.getLong("dt"));

                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat(ApplicationConstants.LONG_DATETIME_A);
                        forecast.condition.setDate(dateFormat.parse(jCurrent.getString("dt_txt")));
                    } catch (ParseException e) {
                        e.printStackTrace();
                        forecast.condition.setDate(new Date());
                    }

                    JSONObject jMain = jCurrent.getJSONObject("main");
                    forecast.condition.setHumidity(jMain.getDouble("humidity"));
                    forecast.condition.setPressure(jMain.getDouble("pressure"));
                    forecast.temperature.setTemperature(jMain.getDouble("temp"));
                    forecast.temperature.setMinTemperature(jMain.getDouble("temp_min"));
                    forecast.temperature.setMaxTemperature(jMain.getDouble("temp_max"));

                    try {
                        forecast.temperature.setKfTemperature(jMain.getDouble("temp_kf"));
                    } catch (JSONException e) {
                        Log.d(TAG, "No value for temp_kf");
                    }

                    JSONObject jDetails = jCurrent.getJSONArray("weather").getJSONObject(0);
                    forecast.condition.setId(jDetails.getInt("id"));
                    forecast.condition.setDescription(jDetails.getString("description"));
                    forecast.condition.setCondition(jDetails.getString("main"));
                    forecast.condition.setIcon(jDetails.getString("icon"));

                    try {
                        JSONObject jWind = jCurrent.getJSONObject("wind");
                        forecast.wind.setSpeed(jWind.getDouble("speed"));
                        forecast.wind.setDegree(jWind.getDouble("deg"));
                    }
                    catch (JSONException e)
                    {
                        Log.d(TAG, "No value for wind");
                    }

                    try {
                        JSONObject jCloud = jCurrent.getJSONObject("clouds");
                        forecast.clouds.setAll(jCloud.getInt("all"));
                    }
                    catch (JSONException e)
                    {
                        Log.d(TAG, "No value for clouds");
                    }

                    try {
                        JSONObject jRain = jCurrent.getJSONObject("rain");
                        forecast.rain.setAmount(jRain.getDouble("3h"));
                    } catch (JSONException e) {
                        Log.d(TAG, "No value for rain");
                    }

                    try {
                        JSONObject jSnow = jCurrent.getJSONObject("snow");
                        forecast.snow.setAmount(jSnow.getDouble("3h"));
                    } catch (JSONException e) {
                        Log.d(TAG, "No value for snow");
                    }

                    weather.forecasts.add(forecast);
                }
            } else {
                weather.setMessage(object.getString("message"));
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            weather = new HourlyForecastWeather();
            weather.setCod(400);
            weather.setMessage(e.getMessage());
        }

        return weather;
    }
}
