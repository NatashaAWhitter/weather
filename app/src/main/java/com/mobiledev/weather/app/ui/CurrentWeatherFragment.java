package com.mobiledev.weather.app.ui;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.*;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobiledev.weather.app.network.AlarmService;
import com.mobiledev.weather.app.ApplicationConstants;
import com.mobiledev.weather.app.R;
import com.mobiledev.weather.app.data.CurrentWeather;
import com.mobiledev.weather.app.utils.LocationUtils;
import com.mobiledev.weather.app.utils.WeatherUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Natasha Whitter
 * @since 10/05/2014
 */
public class CurrentWeatherFragment extends Fragment
{
    private static final String TAG = "CurrentWeatherActivity";

    private RelativeLayout rlBackground;
    private TextView tvCity;
    private TextView tvCountry;
    private ImageView ivWeatherIcon;
    private TextView tvTemp;
    private TextView tvWindSpeed;
    private TextView tvWindDegree;
    private TextView tvClouds;
    private TextView tvHumidity;
    private TextView tvPressure;
    private TextView tvSunrise;
    private TextView tvSunset;
    private TextView tvMaxTemp;
    private TextView tvMinTemp;
    private TextView tvRain;
    private TextView tvSnow;
    private TextView tvLastUpdated;
    private ImageView ivDetectLocation;

    private SharedPreferences sharedPref;
    private CurrentWeather weather;
    public static int id = 0;
    private Context context;
    public static ProgressDialog loadingDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.activity_current_weather, container, false);

        id = this.getId();
        context = this.getActivity();

        tvCity = (TextView) view.findViewById(R.id.current_city);
        tvCountry = (TextView) view.findViewById(R.id.current_country);
        ivWeatherIcon = (ImageView) view.findViewById(R.id.current_image);
        tvTemp = (TextView) view.findViewById(R.id.current_temp);
        tvWindSpeed = (TextView) view.findViewById(R.id.wind_speed);
        tvWindDegree = (TextView) view.findViewById(R.id.wind_direction);
        tvClouds = (TextView) view.findViewById(R.id.cloud_coverage);
        tvHumidity = (TextView) view.findViewById(R.id.humidity);
        tvPressure = (TextView) view.findViewById(R.id.pressure);
        tvSunrise = (TextView) view.findViewById(R.id.sunrise);
        tvSunset = (TextView) view.findViewById(R.id.sunset);
        tvMaxTemp = (TextView) view.findViewById(R.id.max_temp);
        tvMinTemp = (TextView) view.findViewById(R.id.mini_temp);
        tvRain = (TextView) view.findViewById(R.id.rain);
        tvSnow = (TextView) view.findViewById(R.id.snow);
        tvLastUpdated = (TextView) view.findViewById(R.id.last_updated);
        ivDetectLocation = (ImageView) view.findViewById(R.id.gps_picture);
        rlBackground = (RelativeLayout) view.findViewById(R.id.list);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        ivDetectLocation.setVisibility(View.GONE);

        /**
         * If there's weather in the saved state, then populate the screen with this weather, else show the dialog until
         * weather is received from OpenWeatherMap
         */
        if (savedInstanceState != null) {
            this.weather = (CurrentWeather) savedInstanceState.getSerializable("weather");
            if (weather != null) {
                Log.d(TAG, "Weather received from intent");
                processFinish(weather);
            }
        }
        else
        {
            Log.d(TAG, "Dialog shown while loading weather data");
            loadingDialog = new ProgressDialog(context);
            loadingDialog.setCancelable(true);
            loadingDialog.setMessage("Loading weather...");
            loadingDialog.show();
        }

        return view;
    }

    /**
     * When called, start the weather service depending if the user has enabled auto update or set an update frequency
     * in preferences
     */
    @Override
    public void onResume() {
        super.onResume();
        boolean autoUpdate = sharedPref.getBoolean(context.getString(R.string.pref_auto_update_key), true);
        int updateFrequency = Integer.parseInt(sharedPref.getString(context.getString(R.string.pref_update_frequency_key), "0"));
        if (autoUpdate || updateFrequency != -1) {
            Intent intent = new Intent(getActivity(), AlarmService.class);
            intent.putExtra(context.getString(R.string.pref_weather_type), 0);
            context.startService(intent);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_refresh, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_refresh:
                Log.d(TAG, "Refresh button");
                context.startService(new Intent(getActivity(), AlarmService.class));
                return true;
            case R.id.action_settings:
                Log.d(TAG, "Settings button");
                Intent intent = new Intent(context, PrefsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_city:
                Log.d(TAG, "City select button");
                Intent intent1 = new Intent(context, SelectCityActivity.class);
                startActivity(intent1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Save the weather data
     * @param outState
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("weather", weather);
    }

    /**
     * Stop the server from running (onPause is guaranteed to be called before finish)
     */
    @Override
    public void onPause() {
        super.onPause();
        context.stopService(new Intent(getActivity(), AlarmService.class));
    }

    /**
     * Update views with new weather information depending if the weather code is 200
     */
    public void processFinish(CurrentWeather weather) {
        if (weather.getCod() == 200) {
            // If the loading dialog isn't null, the dismiss it
            if (loadingDialog != null) { loadingDialog.dismiss(); }
            String selectedLocation = sharedPref.getString(context.getString(R.string.pref_selected_location), "");
            boolean useCurrentLocation = sharedPref.getBoolean(context.getString(R.string.pref_current_location_key), true);

            // if the user is using their current location or no select location is found, then show the GPS logo
            if (useCurrentLocation || selectedLocation.isEmpty()) {
                ivDetectLocation.setVisibility(View.VISIBLE);
            }
            else
            {
                ivDetectLocation.setVisibility(View.GONE);
            }

            rlBackground.setBackgroundColor(WeatherUtils.getBackgroundColor(weather.temperature.getTemperature(), context));

            tvCity.setText(weather.location.getCity());
            tvCountry.setText(weather.location.getCountry());
            tvTemp.setText(WeatherUtils.getTemperature(weather.temperature.getTemperature(), context));
            ivWeatherIcon.setImageDrawable(context.getResources().getDrawable(WeatherUtils.getIcon(weather.condition.getIcon())));
            tvWindSpeed.setText(WeatherUtils.getWindSpeed(weather.wind.getSpeed(), context));
            tvWindDegree.setText(WeatherUtils.getDegrees(weather.wind.getDegree()));
            tvClouds.setText(Integer.toString(weather.clouds.getAll()) + "%");
            tvHumidity.setText(Integer.toString((int) Math.round(weather.condition.getHumidity())) + "%");
            tvPressure.setText(Integer.toString(WeatherUtils.getPressure(weather.condition.getPressure(), context)));

            SimpleDateFormat minuteHour = new SimpleDateFormat(ApplicationConstants.DEFAULT_TIME_12HR, getResources().getConfiguration().locale);
            tvSunrise.setText(minuteHour.format(new Date(weather.location.getSunrise())));
            tvSunset.setText(minuteHour.format(new Date(weather.location.getSunset())));

            tvMaxTemp.setText(WeatherUtils.getTemperature(weather.temperature.getMaxTemperature(), context));
            tvMinTemp.setText(WeatherUtils.getTemperature(weather.temperature.getMinTemperature(), context));
            tvRain.setText(Integer.toString((int) Math.round(weather.rain.getAmount())));
            tvSnow.setText(Integer.toString((int) Math.round(weather.snow.getAmount())));

            // Depending if the last update was today or yesterday, then update the fragment with the correct date format
            SimpleDateFormat dateFormat;
            if (LocationUtils.isToday(weather.location.getDatetime()))
            {
                dateFormat = new SimpleDateFormat(ApplicationConstants.DEFAULT_TIME_12HR, getResources().getConfiguration().locale);
            }
            else
            {
                dateFormat = new SimpleDateFormat(ApplicationConstants.DEFAULT_DATETIME, getResources().getConfiguration().locale);
            }

            tvLastUpdated.setText(dateFormat.format(new Date(weather.location.getDatetime())));

            // If the user has set the application to not update automatically, then stop the service
            boolean autoUpdate = sharedPref.getBoolean(context.getString(R.string.pref_auto_update_key), true);
            int updateFrequency = Integer.parseInt(sharedPref.getString(context.getString(R.string.pref_update_frequency_key), "0"));
            if (!autoUpdate || updateFrequency == -1) {
                context.stopService(new Intent(getActivity(), AlarmService.class));
            }
        }
    }
}
