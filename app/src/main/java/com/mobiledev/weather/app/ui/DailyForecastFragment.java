package com.mobiledev.weather.app.ui;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.AbsListView;
import android.widget.AdapterView;
import com.mobiledev.weather.app.R;
import com.mobiledev.weather.app.data.DailyForecast;
import com.mobiledev.weather.app.data.DailyForecastWeather;
import com.mobiledev.weather.app.data.HourlyForecast;
import com.mobiledev.weather.app.data.HourlyForecastWeather;
import com.mobiledev.weather.app.network.AlarmService;
import com.mobiledev.weather.app.ui.adapter.DailyForecastAdapter;

/**
 * @author Natasha Whitter
 * @since 18/05/2014
 */
public class DailyForecastFragment extends ListFragment implements AdapterView.OnItemClickListener{

    public static final String TAG = "DailyForecastFragment";
    public Context context;
    public DailyForecastAdapter adapter;
    public DailyForecastWeather forecast;
    public static int id = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        id = this.getId();
        context = getActivity();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_refresh, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_refresh:
                context.startService(new Intent(getActivity(), AlarmService.class));
                return true;
            case R.id.action_settings:
                Intent intent = new Intent(context, PrefsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_city:
                Intent intent1 = new Intent(context, SelectCityActivity.class);
                startActivity(intent1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Intent intent = new Intent(getActivity(), AlarmService.class);
        intent.putExtra(context.getString(R.string.pref_weather_type), 1);
        context.startService(intent);
        // Listen for clicks on list
        getListView().setOnItemClickListener(this);
        getListView().setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        getListView().setSelector(R.drawable.fragment_list_selector);
    }

    /**
     * Stop service
     */
    @Override
    public void onPause() {
        super.onPause();
        context.stopService(new Intent(getActivity(), AlarmService.class));
    }

    /**
     * If the weather variable is 200, then update the weather list
     * @param weather
     */
    public void processFinish(DailyForecastWeather weather) {
        if (weather.getCod() == 200)
        {
            forecast = weather;
            adapter = new DailyForecastAdapter(context, weather);
            setListAdapter(adapter);
            Log.d(TAG, weather.toString());
        }
    }

    /**
     * When an item is clicked in the list, send this data to a new activity using bundle
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        getListView().setItemChecked(position, true);
        getListView().setSelection(position);
        getListView().setSelected(true);

        Bundle bundle = new Bundle();
        bundle.putSerializable(getString(R.string.intent_forecast), (DailyForecast) parent.getItemAtPosition(position));
        bundle.putString(getString(R.string.intent_city), forecast.location.getCity());
        bundle.putString(getString(R.string.intent_country), forecast.location.getCountry());
        bundle.putLong(getString(R.string.intent_sunrise), forecast.location.getSunrise());
        bundle.putLong(getString(R.string.intent_sunset), forecast.location.getSunset());

        Intent intent = new Intent(context, DailyWeatherFragment.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}